const { emit } = require('../models/Message');
const Message = require('../models/Message');
const MessageFlag = require("../models/MessageFlag");
const Room = require("../models/Room");


var users = {}
var rooms = {}

exports._socket = (io, socket) => {
    socket.on('init', (data) => {
        // Room Ids
        // if(data.hasOwnProperty('rooms')) {
        //   var room_ids = data.rooms;
        //   rooms[data.id] = room_ids;
        // }

        // Current User
        var user = {...data, online: 1, last_update: new Date()};
        users[socket.id] = user;

        // Get room ids with is_seen = false and participants has current_user in it. n

        // Emit Online Users
        io.sockets.emit("online_users", users);
        console.log(user.id, '-- joined');
    });


    /*
    
        {
            users: [
                {user1}, {user2}
            ],
            room_id: 1,

        }
    
    */
    socket.on('join', async (data) => {

        // Check if Room ID already Exists
        // If not create a room
        var room = await Room.findOne({ id: data.room_id });
        socket.join(data.room_id);

        if(room == null) {

            var room = new Room({
                id: data.room_id,
                participants: data.users,
                options: {
                    name: data.room_name || null,
                    last_activity: new Date(),
                    type: data.users.length == 2 ? 'direct' : 'group'
                }
            });


            await room.save();
            io.to(data.room_id).emit("messages", null);

        } else {

            Message.aggregate([
              {
                $lookup: {
                  from: "messageflags",
                  localField: "_id",
                  foreignField: "message_id",
                  as: "flag",
                },
              },
              {
                $match: {
                  room_id: {
                    $eq: data.room_id + '',
                  },
                },
              },
              {
                $sort: {
                  created_at: -1
                }
              },
              {
                $limit: 20
              }
            ]).sort({
              created_at: 1
            })
              .exec((err, result) => {
                  //console.log(err)
                  //console.log(result)
                if (err) 
                    io.to(data.room_id).emit("messages", {
                        type: 'history',
                        prepend: false,
                        messages: []
                    });
                
                    
                io.to(data.room_id).emit("messages", {
                    type: "history",
                    prepend: false,
                    messages: result,
                });
              });


            // The moment he/she joins flags will be updated to seen
            MessageFlag.updateMany(
                { room_id: data.room_id, user_id: users[socket.id].id },
                {
                    $set: {
                        is_seen: true,
                    },
                }
            ).exec();
            

        }
    })


    // executed when user clicked on the body
    socket.on('seen', (data) => {
        MessageFlag.updateMany(
           { room_id: data.room_id, user_id: users[socket.id].id },
           {
             $set: {
               is_seen: true,
             },
           }
        ).exec();
    })

    /*
    
        {
            room_id: 1,
            message: string,

        }
    
    */

    socket.on('send_message', async (data) => {
        var room = await Room.findOne({ id: data.room_id });
        let message = new Message({
          room_id: data.room_id,
          from: users[socket.id].id,
          msg: data.msg,
          created_at: new Date(),
          msg_type: data.type,
          options: {
            type: data.type || 'text',
            ext: data.ext,
            name: data.name,
          },
        })
        

        await message.save();


        // Inserting of Flags
        let flags = [];
        for (var p of room.participants) {
            if(p != users[socket.id].id) {
                 flags.push({
                   room_id: data.room_id,
                   message_id: message._id,
                   from: users[socket.id].id,
                   user_id: p,
                 });

                 socket.broadcast.emit("notify-user-" + p, {
                   room_id: data.room_id,
                   from: users[socket.id],
                   msg: data.msg,
                   created_at: new Date(),
                 });

                 //console.log("notify-user-" + p);

            }

           
            // io.sockets.emit("room-activity-" + p, {
            //     room_id: data.room_id,
            //     from: users[socket.id],
            //     msg: data.msg,
            //     created_at: new Date(),
            // });
        }

        await MessageFlag.insertMany(flags);

        var _flags = await MessageFlag.find({ message_id: message._id });

        ////console.log({ ...message._doc, flag: _flags }, '-- sent');

        io.to(data.room_id).emit("messages", {
          type: "new",
          message: { ...message._doc, flag: _flags },
        });

        ////console.log(' -- pased emit --');

        await Room.updateOne(
            {room_id: data.room_id},
            {
                $set: {
                    last_activity: new Date()
                }
            }
        )
    })

    socket.on('get_history', async (data) => {
        Message.aggregate([
          {
            $lookup: {
              from: "messageflags",
              localField: "_id",
              foreignField: "message_id",
              as: "flag",
            },
          },
          {
            $match: {
              room_id: {
                $eq: data.room_id + "",
              },
            },
          },
          {
            $sort: {
              created_at: -1,
            },
          },
          {
            $limit: 20 + (data.page * 20),
          },
          {
            $skip: data.page * 20,
          },
        ])
          .sort({
            created_at: 1,
          })
          .exec((err, result) => {
            //console.log(err)
            //console.log(result)
            if (err)
              io.to(data.room_id).emit("messages", {
                type: "history",
                prepend: true,
                messages: [],
              });

            io.to(data.room_id).emit("messages", {
              type: "history",
              prepend: true,
              messages: result,
            });

            console.log(result, ' -- get jostpru' , data);
          });
    })

    socket.on('typing', (data) => {
      socket.to(data.room_id).emit("is_typing", {
        msg: data.msg
      });
    })

    socket.on('disconnect', (data) => {
        delete users[socket.id];
        io.sockets.emit("online_users", users);
    })
}


const Message = require('../models/Message');
const Room = require("../models/Room");

var user = {};
var msg = {}
var current_room_id = null;

const getRoomId = (participants, data) => {
    // Sorted to get constant result of room id
    var sorted = participants.sort();
    var name = sorted.join('-r-');

    if(data != null) {
        if (data !== null && Object.keys(data).indexOf("delimeter") >= 0)
          name += data.delimeter;
        if (data !== null && Object.keys(data).indexOf("item_id") >= 0)
          name += data.item_id; 
    }

    return name;
}

exports.userOnJoin = (io, socket, data) => {
    user[socket.id] = {...data, online: 1};
    io.sockets.emit("online", user);

    console.log(user[socket.id].id, ' has joined');
}

exports.directOnJoin = (io, socket, user_id, data) => {
    var room_id = getRoomId([user_id, user[socket.id].id], data);
    //console.log(room_id, '--- join please check me')
    socket.join(room_id);
    current_room_id = room_id;

    var messages = Message.find({ room_id: room_id }).sort({ created_at: 1 }).limit(20);
    
    messages.exec(function(err, msgs) {
        console.log(msgs);
        if(err) io.to(room_id).emit("msg", null);
        if(msgs != null && msgs.length != 0) socket.emit("msg", msgs);
    })

    
}

exports.userOnDisconnect = (io, socket, data) => {
    //console.log(user[socket.id], ' has disconnected');
    delete user[socket.id];
    io.sockets.emit("online", user);
}

exports.directOnMessageSent = async (io, socket, data) => {
    var room_id = '';
    var participants = []

    
    if(data.socket_id == undefined || data.socket_id == null) {
        participants = [user[socket.id].id, data.to];
    } else {
        participants = [user[socket.id].id, user[data.socket_id].id];
    }
    
    var room_id = getRoomId(participants, data)
    
    console.log({...data, room: room_id}, 'msg-sent');

    // Create Room if doesn't exists.
    if (msg[room_id] == null || msg[room_id] == undefined) {
      let room = new Room({
        id: room_id,
        participants: participants,
        options: {
            name: data.room_name || null
        }
      });


      await room.save();

    }

    let msgObj = {
      room_id: room_id,
      from: user[socket.id].id,
      msg: data.msg,
      created_at: new Date(),
      options: {},
    };

    let message = new Message(msgObj)

    await message.save();
    
    // 

    io.to(room_id).emit("msg", msgObj);
    io.emit("received-" + data.to, { ...msgObj, username: user[socket.id].username});

    // if(data.socket_id != null) {
    //     io.to(data.socket_id).emit("received", socket.id);
    // }
}

exports.isTyping = (io, socket, msg) => {
    socket.to(current_room_id).emit('is_typing', msg);
}


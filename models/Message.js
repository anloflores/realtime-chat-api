const mongoose = require('mongoose')

var MessagesSchema = mongoose.Schema({
    room_id : {
        type: String,
        required: true
    },
    from: {
        type: String,
        required: true,
    },
    is_direct: {
        type: Boolean,
        default: true
    },
    msg: {
        type: String,
        required: true
    },
    msg_type: {
        type: String,
        default: 'text'
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    options: {
        type: mongoose.Schema.Types.Mixed
    }
})

module.exports = mongoose.model("Messages", MessagesSchema);
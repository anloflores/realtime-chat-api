const mongoose = require('mongoose')

var MessageFlagSchema = mongoose.Schema({
    room_id : {
        type: String,
        required: true
    },
    message_id : {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    from: {
        type: String,
        required: true,
    },
    user_id: {
        type: String,
        required: true
    },
    is_seen: {
        type: Boolean,
        default: false
    }
})

module.exports = mongoose.model("MessageFlags", MessageFlagSchema);
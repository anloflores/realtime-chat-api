const mongoose = require('mongoose')

var RoomsSchema = mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    participants: {
        type: Array,
        required: true
    },
    last_activity: {
        type: Date,
        default: new Date()
    },
    options: {
        type: mongoose.Schema.Types.Mixed
    }
})

module.exports = mongoose.model("Rooms", RoomsSchema);
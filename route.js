const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");

async function authenticated(req, res, next) {
    let authorization = req.headers["authorization"];
    let accessToken = authorization.split(" ")[1];
    
    await jwt.verify(accessToken, process.env.ACCESS_SECRET_TOKEN, (err, user) => {
        if(err) res.sendStatus(403);
        req.user = user;
        next();
    });
}

module.exports = router;

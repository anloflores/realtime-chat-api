var cors = require("cors");
var app = require("express")();
var http = require("http").createServer(app);
const Message = require("./models/Message");
const MessageFlag = require("./models/MessageFlag");
var { userOnJoin, directOnJoin, userOnDisconnect, directOnMessageSent, isTyping } = require("./controllers/message");
var { _socket } = require("./controllers/socket.js");

var mongoose = require('mongoose');

const io = require("socket.io")(http, {
  cors: {
    origin: "*",
  },
});

var corsOptions = {
  origin: "*",
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(cors(corsOptions));

app.get('/', async (req, res) => {
  res.send({msg: 'hello world!'});
})

mongoose.connect(
  "mongodb://localhost:27017/marketplace",
  { useNewUrlParser: true, useUnifiedTopology: true },
  function (error) {
    if (error) console.log(error);
    console.log("connected to db");
  }
);

var user = {}
var msg = {}
io.on("connection", (socket) => {

    // socket.on("user", (data) => userOnJoin(io, socket, data));
    // socket.on('direct_join', (socket_id, data) => directOnJoin(io, socket, socket_id, data))
    // socket.on('disconnect', (data) => userOnDisconnect(io, socket, data))
    // socket.on('direct', (data) => directOnMessageSent(io, socket, data))
    // socket.on('typing', (msg) => isTyping(io, socket, msg))

    _socket(io, socket);
});

// Connect to DB
// mongoose.connect(process.env.MONGO_DB, { useNewUrlParser: true, useUnifiedTopology: true })
http.listen(3000, function () {
  console.log("liste`ning on *:3000");
});